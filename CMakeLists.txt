# ##############################################################################
# SatNOGS-COMMS MCU software
#
# Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GNU General Public License v3.0 or later
# ##############################################################################

cmake_minimum_required(VERSION 3.20)

set(BOARD_ROOT ${CMAKE_CURRENT_LIST_DIR})

find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})

project(
  satnogs-comms
  LANGUAGES C CXX
  VERSION 1.5.0)

set(CMAKE_C_STANDARD 17)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD_REQUIRED True)
set(CMAKE_CXX_STANDARD_REQUIRED True)

list(APPEND CMAKE_MODULE_PATH ${ZEPHYR_BASE}/modules/nanopb)
include(nanopb)

configure_file(src/version.hpp.in ${CMAKE_BINARY_DIR}/include/version.hpp)

target_include_directories(app PRIVATE ${CMAKE_BINARY_DIR}/include src)

# This string ends up getting printed in the device console
if(NOT DEFINED FROM_WHO)
  set(FROM_WHO Zephyr)
endif()

target_compile_definitions(app
                           PRIVATE "-DMCUBOOT_HELLO_WORLD_FROM=\"${FROM_WHO}\"")
target_include_directories(app PRIVATE "${PROJECT_BINARY_DIR}")

# Set the HW_VERSION variable required by the libsatnogs-comms See
# https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/tags
# for more details
set(HW_VERSION
    ${BOARD_REVISION}
    CACHE STRING "")

include(FetchContent)
FetchContent_Declare(
  satnogs-comms-ctrl-lib
  SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/libsatnogs-comms" BINARY_DIR
  "${CMAKE_CURRENT_BINARY_DIR}/libsatnogs-comms")
FetchContent_MakeAvailable(satnogs-comms-ctrl-lib)

zephyr_nanopb_sources(
  app satnogs-comms-proto/satnogs-comms.proto
  satnogs-comms-proto/telecommand.proto satnogs-comms-proto/telemetry.proto
  satnogs-comms-proto/ota.proto satnogs-comms-proto/rf.proto)

set(app_sources
    # cmake-format: sortable
    src/callbacks.cpp
    src/errors.cpp
    src/io.cpp
    src/main.cpp
    src/msg_arbiter.cpp
    src/ota.cpp
    src/settings.cpp
    src/telemetry.cpp
    src/tests/test.cpp)

target_sources(app PRIVATE ${app_sources})
target_link_libraries(app PRIVATE libsatnogs-comms)

set(CMAKE_EXPORT_COMPILE_COMMANDS true)

if(CMAKE_EXPORT_COMPILE_COMMANDS)
  set(CMAKE_CXX_STANDARD_INCLUDE_DIRECTORIES
      ${CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES})
endif()
