/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once
#include <satnogs-comms/radio.hpp>
#include <zephyr/kernel.h>

class settings
{
public:
  static constexpr const char *setting_boot_count    = "/lfs1/boot_cnt";
  static constexpr const char *setting_uhf_tx_freq   = "/lfs1/uhf-tx-freq";
  static constexpr const char *setting_uhf_rx_freq   = "/lfs1/uhf-rx-freq";
  static constexpr const char *setting_sband_tx_freq = "/lfs1/sband-tx-freq";
  static constexpr const char *setting_sband_rx_freq = "/lfs1/sband-rx-freq";
  static constexpr const char *setting_uhf_tx_en     = "/lfs1/uhf-tx-en";
  static constexpr const char *setting_sband_tx_en   = "/lfs1/sband-tx-en";
  static constexpr const char *setting_tlm_period    = "/lfs1/tlm-period";

  static settings &
  get_instance()
  {
    static settings instance;
    return instance;
  }

  /* Singleton */
  settings(settings const &) = delete;

  void
  operator=(settings const &) = delete;

  uint32_t
  boot_cnt();
  void
  incr_boot_cnt();

  float
  tx_freq(satnogs::comms::radio::interface iface);
  void
  set_tx_freq(satnogs::comms::radio::interface iface, float freq);

  float
  rx_freq(satnogs::comms::radio::interface iface);
  void
  set_rx_freq(satnogs::comms::radio::interface iface, float freq);

  bool
  tx_enabled(satnogs::comms::radio::interface iface);
  void
  set_tx_enable(satnogs::comms::radio::interface iface, bool en);

  bool
  deployed();

  uint32_t
  periodic_tlm_secs();
  void
  set_periodic_tlm_secs(uint32_t secs);

protected:
  settings();

private:
  mutable struct k_mutex m_mtx;

  int
  read32(const char *fname, uint32_t *x);

  int
  write32(const char *fname, uint32_t x);

  int
  readf(const char *fname, float *x);

  int
  writef(const char *fname, float x);
};