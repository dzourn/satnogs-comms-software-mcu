/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "errors.hpp"

errors::errors() { get_reason_of_reset(); }

void
errors::clear_all()
{
  m_reset_flags_raw.reset();
  errors::set(errors::reset_flag_pos_t::RMVF, true);
  sys_write32(m_reset_flags_raw.to_ulong(), (mem_addr_t)&RCC->RSR);
}

void
errors::set(errors::reset_flag_pos_t position, bool value)
{
  m_reset_flags_raw.set(static_cast<size_t>(position), value);
}

etl::unordered_map<errors::reset_flag_pos_t, bool, errors::map_size>
errors::get_reason_of_reset_map(void) const
{
  return m_flag_status;
}

etl::bitset<32>
errors::get_reason_of_reset_raw(void) const
{
  return m_reset_flags_raw;
}

void
errors::get_reason_of_reset(void)
{
  m_reset_flags_raw = sys_read32((mem_addr_t)&RCC->RSR);

  // Update Reset flags map
  for (auto &[flag_pos, bool_value] : m_flag_status) {
    bool_value = m_reset_flags_raw[static_cast<size_t>(flag_pos)];
  }

  // Clear reset flags
  errors::clear_all();
}