/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "msg_arbiter.hpp"
#include "callbacks.hpp"
#include "ota.hpp"
#include "settings.hpp"
#include "telemetry.hpp"
#include "tests/test.hpp"
#include <pb_decode.h>
#include <pb_encode.h>
#include <satnogs-comms/version.hpp>
#include <string>
#include <tinycrypt/sha256.h>
#include <zephyr/task_wdt/task_wdt.h>

static constexpr std::array<uint8_t, 32>
sha256_key_hex2bytes()
{
  std::array<uint8_t, 32> key = {0};
  static_assert(std::char_traits<char>::length(CONFIG_TLC_SHA256_KEY) == 64,
                "Hash should be exactly 64 hex annotated chars");
  for (unsigned int i = 0; i < 64; i += 2) {
    char s[2]  = {CONFIG_TLC_SHA256_KEY[i], CONFIG_TLC_SHA256_KEY[i + 1]};
    char byte  = (char)strtol(s, NULL, 16);
    key[i / 2] = byte;
  }
  return key;
}

K_THREAD_STACK_DEFINE(msg_arbiter_thread_stack, CONFIG_MSG_ARBITER_STACK_SIZE);
K_THREAD_STACK_DEFINE(async_workq_stack, CONFIG_ASYNC_WORKQUEUE_STACK_SIZE);

namespace sc = satnogs::comms;

static struct k_work_q asynq_workq;

static satnogs_comms proto_recv;
static satnogs_comms proto_send;

static const auto                   &tlc_key = sha256_key_hex2bytes();
static struct tc_sha256_state_struct sha256_state;
static std::array<uint8_t, 32>       sha256_comp;

void
msg_arbiter::push(const msg *m, bool block)
{
  if (block) {
    k_msgq_put(&m_rx_msgq, m, K_FOREVER);
  } else {
    while (k_msgq_put(&m_rx_msgq, m, K_NO_WAIT) != 0) {
      k_msgq_purge(&m_rx_msgq);
    }
  }
}

int
msg_arbiter::pull(msg *m, subsys s, k_timeout_t wait)
{
  switch (s) {
  case subsys::CAN1:
    return k_msgq_get(&m_can1_tx_msgq, m, wait);
  case subsys::RADIO_UHF:
    return k_msgq_get(&m_radio_tx_msgq, m, wait);
  default:
    return -EINVAL;
  }
}

int
msg_arbiter::fwd(msg *m, subsys s, k_timeout_t wait)
{
  switch (s) {
  case subsys::CAN1:
    return k_msgq_put(&m_can1_tx_msgq, m, wait);
  case subsys::RADIO_UHF:
    return k_msgq_put(&m_radio_tx_msgq, m, wait);
  default:
    return -EINVAL;
  }
}
void
msg_arbiter::start()
{
  m_tid = k_thread_create(&m_thread_data, msg_arbiter_thread_stack,
                          K_THREAD_STACK_SIZEOF(msg_arbiter_thread_stack),
                          parse_thread, NULL, NULL, NULL,
                          CONFIG_MSG_ARBITER_PRIO, 0, K_NO_WAIT);
  if (!m_tid) {
    k_oops();
  }
  k_thread_name_set(m_tid, "msg_arbiter");

  const k_work_queue_config cfg = {.name = "asynq_workq", .no_yield = 0};
  k_work_queue_start(&asynq_workq, async_workq_stack,
                     K_THREAD_STACK_SIZEOF(async_workq_stack),
                     CONFIG_ASYNC_WORKQUEUE_PRIO, &cfg);

  k_work_init(&m_test_params.work, &test::exec);
}

void
msg_arbiter::parse_thread(void *arg1, void *arg2, void *arg3)
{
  int          task_wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_MSG_ARBITER,
                                          task_wdt_callback, (void *)k_current_get());
  msg_arbiter &arb         = msg_arbiter::get_instance();
  while (1) {
    task_wdt_feed(task_wdt_id);
    if (k_msgq_get(&arb.m_rx_msgq, &arb.m_msg, K_MSEC(20)) == 0) {

      pb_istream_t stream =
          pb_istream_from_buffer(arb.m_msg.data, arb.m_msg.len);
      bool ok = pb_decode_delimited(&stream, satnogs_comms_fields, &proto_recv);
      if (!ok) {
        continue;
      }

      switch (proto_recv.which_mesages) {
      case satnogs_comms_tlc_tag:
        arb.parse_tlc(proto_recv.mesages.tlc, arb.m_msg.iface);
        break;
      case satnogs_comms_rf_tlc_tag:
        arb.parse_rf_tlc(proto_recv, arb.m_msg.iface);
      default:
        break;
      }
    }
  }
}

msg_arbiter::msg_arbiter()
{
  k_msgq_init(&m_rx_msgq, m_rx_msgq_buffer, sizeof(struct msg), rx_msgq_size);
  k_msgq_init(&m_can1_tx_msgq, m_can1_tx_msgq_buffer, sizeof(struct msg),
              can1_tx_msgq_size);
  k_msgq_init(&m_radio_tx_msgq, m_radio_tx_msgq_buffer, sizeof(struct msg),
              radio_tx_msgq_size);
  k_mutex_init(&m_mtx);
}

void
msg_arbiter::parse_tlc(const telecommands &tlc, subsys iface)
{
  bool has_resp = false;
  switch (tlc.which_telecommand) {
  case telecommands_ping_tag: {
    has_resp = true;
    telemetry::ping(proto_send, tlc);
  } break;
  case telecommands_pwr_tag:
    has_resp = true;
    telemetry::power(proto_send);
    break;
  case telecommands_test_tag:
    has_resp = parse_tlc_test(tlc);
    break;
  case telecommands_tlm_req_tag:
    has_resp = parse_tlm_req(tlc);
    break;
  case telecommands_tlm_periodic_tag:
    has_resp = parse_periodic_tlm_req(tlc);
    break;
  case telecommands_ota_tag:
    has_resp = parse_ota_tlc(tlc);
    break;
  case telecommands_fpga_en_tag:
    has_resp = parse_enable_fpga_req(tlc);
    break;
  case telecommands_radio_en_tag:
    has_resp = parse_enable_radio_req(tlc);
    break;
  case telecommands_sband_en_tag:
    has_resp = parse_enable_sband_req(tlc);
    break;
  case telecommands_uhf_en_tag:
    has_resp = parse_enable_uhf_req(tlc);
    break;
  case telecommands_emmc_en_tag:
    has_resp = parse_enable_emmc_req(tlc);
    break;
  case telecommands_emmc_dir_tag:
    has_resp = parse_emmc_dir_req(tlc);
    break;
  case telecommands_stop_wdt_update_tag:
    has_resp = parse_stop_wdt_update_req(tlc);
    break;
  case telecommands_tx_inhibit_tag:
    has_resp = parse_set_tx_inhibit_req(tlc);
    break;
  case telecommands_freq_tag:
    has_resp = parse_set_freq_req(tlc);
    break;
  case telecommands_tlm_period_tag:
    has_resp = parse_set_tlm_period_req(tlc);
    break;
  default:
    return;
  }

  if (has_resp == false) {
    return;
  }

  auto ostream = pb_ostream_from_buffer(m_msg.data, msg_arbiter::mtu);
  if (pb_encode_delimited(&ostream, satnogs_comms_fields, &proto_send) ==
      false) {
    return;
  }
  m_msg.len = ostream.bytes_written;

  // FIXME interface handling needs revising
  switch (iface) {
  case subsys::CAN1:
    k_msgq_put(&m_can1_tx_msgq, &m_msg, K_FOREVER);
    break;
  case subsys::RADIO_UHF:
    k_msgq_put(&m_radio_tx_msgq, &m_msg, K_FOREVER);
  default:
    return;
  }
}

void
msg_arbiter::parse_rf_tlc(const satnogs_comms &msg, subsys iface)
{

  const auto &rfmsg = msg.mesages.rf_tlc.messages.tlc;

  /* Check the key first */
  tc_sha256_init(&sha256_state);
  tc_sha256_update(&sha256_state, rfmsg.key.bytes, rfmsg.key.size);
  tc_sha256_final(sha256_comp.data(), &sha256_state);
  if (sha256_comp != tlc_key) {
    return;
  }
  parse_tlc(rfmsg.tlc, iface);
}

bool
msg_arbiter::parse_tlc_test(const telecommands &tlc)
{
  const auto &test_req                   = tlc.telecommand.test;
  proto_send.which_mesages               = satnogs_comms_tlm_tag;
  proto_send.mesages.tlm.which_telemetry = telemetry_resp_ack_resp_tag;
  auto &ack = proto_send.mesages.tlm.telemetry.ack_resp;
  auto &t   = test::get_instance();

  switch (test_req.which_tests) {
  case test_req_start_tag: {
    if (t.test_valid(test_req.tests.start.id) == false) {
      ack.success = false;
      ack.err     = error_UNKNOWN_TEST;
      return true;
    }
    m_test_params.test_id = test_req.tests.start.id;
    m_test_params.param0  = test_req.tests.start.param0;
    m_test_params.param1  = test_req.tests.start.param1;

    int ret = k_work_submit_to_queue(&asynq_workq, &m_test_params.work);
    if (ret >= 0) {
      ack.success    = true;
      ack.err        = error_NO_ERROR;
      ack.error_code = ret;
    } else {
      ack.success    = false;
      ack.err        = error_EXEC_FAIL;
      ack.error_code = ret;
    }
  } break;
  /* Stop any running test */
  case test_req_stop_tag:
    t.stop();
    ack.success = true;
    ack.err     = error_NO_ERROR;
    break;
  default:
    ack.success = false;
    ack.err     = error_INVALID_CMD;
  }
  return true;
}

bool
msg_arbiter::parse_tlm_req(const telecommands &tlc)
{
  const auto &tlm_req      = tlc.telecommand.tlm_req;
  proto_send.which_mesages = satnogs_comms_tlm_tag;
  telemetry::tlm(proto_send, tlm_req.type);
  return true;
}

bool
msg_arbiter::parse_stop_wdt_update_req(const telecommands &tlc)
{
  const auto &req = tlc.telecommand.stop_wdt_update;
  if (req.stop_wdt_update) {
    k_msleep(CONFIG_WATCHDOG_PERIOD_SYS + 1000);
  }
  return true;
}

bool
msg_arbiter::parse_set_freq_req(const telecommands &tlc)
{
  const auto &req = tlc.telecommand.freq;
  auto       &s   = settings::get_instance();

  auto iface = sc::radio::interface::UHF;

  switch (req.iface) {
  case radio_iface_UHF:
    iface = sc::radio::interface::UHF;
    break;
  case radio_iface_SBAND:
    iface = sc::radio::interface::SBAND;
    break;
  default:
    return false;
  }

  switch (req.direction) {
  case radio_dir_RX:
    s.set_rx_freq(iface, req.freq);
    break;
  case radio_dir_TX:
    s.set_tx_freq(iface, req.freq);
    break;
  default:
    return false;
  }
  return true;
}

bool
msg_arbiter::parse_set_tx_inhibit_req(const telecommands &tlc)
{
  const auto &req = tlc.telecommand.tx_inhibit;
  auto       &s   = settings::get_instance();
  switch (req.iface) {
  case radio_iface_UHF:
    s.set_tx_enable(sc::radio::interface::UHF, !req.enable);
    break;
  case radio_iface_SBAND:
    s.set_tx_enable(sc::radio::interface::SBAND, !req.enable);
    break;
  default:
    return false;
  }
  return true;
}

bool
msg_arbiter::parse_set_tlm_period_req(const telecommands &tlc)
{
  const auto &req = tlc.telecommand.tlm_period;
  auto       &s   = settings::get_instance();
  s.set_periodic_tlm_secs(etl::clamp<uint32_t>(req.period, 10, 1000));
  return true;
}

bool
msg_arbiter::parse_enable_emmc_req(const telecommands &tlc)
{
  auto &req  = tlc.telecommand.emmc_en;
  auto &emmc = sc::board::get_instance().emmc();
  if (req.enable_emmc) {
    emmc.enable(false);
    emmc.enable(true);
  } else {
    emmc.enable(false);
  }
  return true;
}

bool
msg_arbiter::parse_emmc_dir_req(const telecommands &tlc)
{
  auto &req  = tlc.telecommand.emmc_dir;
  auto &emmc = sc::board::get_instance().emmc();
  switch (req.set_dir) {
  case dir_interface::dir_interface_emmc_to_mcu: {
    emmc.enable(false);
    emmc.enable(true);
    emmc.set_dir(sc::emmc::dir::MCU);
  } break;
  case dir_interface::dir_interface_emmc_to_fpga: {
    emmc.enable(false);
    emmc.enable(true);
    emmc.set_dir(sc::emmc::dir::FPGA);
  } break;
  default:
    break;
  }
  return true;
}

bool
msg_arbiter::parse_enable_fpga_req(const telecommands &tlc)
{
  auto &req  = tlc.telecommand.fpga_en;
  auto &fpga = sc::board::get_instance().fpga();
  if (req.enable_fpga) {
    fpga.enable(true);
  } else {
    fpga.enable(false);
  }
  return true;
}

bool
msg_arbiter::parse_enable_radio_req(const telecommands &tlc)
{
  auto &req   = tlc.telecommand.radio_en;
  auto &radio = sc::board::get_instance().radio();
  if (req.enable_radio) {
    radio.enable(true);
  } else {
    radio.enable(false);
  }
  return true;
}

bool
msg_arbiter::parse_enable_sband_req(const telecommands &tlc)
{
  auto &req   = tlc.telecommand.sband_en;
  auto &radio = sc::board::get_instance().radio();
  if (req.enable_sband) {
    radio.enable(sc::radio::interface::SBAND, true);
  } else {
    radio.enable(sc::radio::interface::SBAND, false);
  }
  return true;
}

bool
msg_arbiter::parse_enable_uhf_req(const telecommands &tlc)
{
  auto &req   = tlc.telecommand.uhf_en;
  auto &radio = sc::board::get_instance().radio();
  if (req.enable_uhf) {
    radio.enable(sc::radio::interface::UHF, true);
  } else {
    radio.enable(sc::radio::interface::UHF, false);
  }
  return true;
}

bool
msg_arbiter::parse_periodic_tlm_req(const telecommands &tlc)
{
  auto &req = tlc.telecommand.tlm_periodic;
  auto &tlm = telemetry::get_instance();

  if (req.enable) {
    tlm.enable(req.type, req.period_ms);
  } else {
    tlm.stop();
  }

  proto_send.which_mesages               = satnogs_comms_tlm_tag;
  proto_send.mesages.tlm.which_telemetry = telemetry_resp_ack_resp_tag;
  auto &ack      = proto_send.mesages.tlm.telemetry.ack_resp;
  ack.success    = true;
  ack.error_code = error_NO_ERROR;
  ack.error_code = 0;
  return true;
}

bool
msg_arbiter::parse_ota_tlc(const telecommands &tlc)
{
  const auto &ota_msg = tlc.telecommand.ota;
  auto       &ota     = ota::get_instance();

  proto_send.which_mesages               = satnogs_comms_tlm_tag;
  proto_send.mesages.tlm.which_telemetry = telemetry_resp_ota_resp_tag;

  switch (ota_msg.which_messages) {
  case ota_tlc_request_tag:
    ota.begin(ota_msg.messages.request,
              proto_send.mesages.tlm.telemetry.ota_resp);
    break;
  case ota_tlc_data_tag:
    ota.packet(ota_msg.messages.data,
               proto_send.mesages.tlm.telemetry.ota_resp);
    break;
  case ota_tlc_finish_tag:
    ota.finish(ota_msg.messages.finish,
               proto_send.mesages.tlm.telemetry.ota_resp);
    break;
  default:
    return false;
  }
  return true;
}
