/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */
#include <cstdint>
#include <etl/bitset.h>
#include <etl/unordered_map.h>
#include <zephyr/arch/common/sys_io.h>
#include <zephyr/kernel.h>

class errors
{
public:
  static constexpr uint32_t map_size = 11;

  enum class reset_flag_pos_t : size_t
  {
    RMVF      = RCC_RSR_RMVF_Pos,
    CPURSTF   = RCC_RSR_CPURSTF_Pos,
    D1RSTF    = RCC_RSR_D1RSTF_Pos,
    D2RSTF    = RCC_RSR_D2RSTF_Pos,
    BORRSTF   = RCC_RSR_BORRSTF_Pos,
    PINRSTF   = RCC_RSR_PINRSTF_Pos,
    PORRSTF   = RCC_RSR_PORRSTF_Pos,
    SFTRSTF   = RCC_RSR_SFTRSTF_Pos,
    IWDG1RSTF = RCC_RSR_IWDG1RSTF_Pos,
    WWDG1RSTF = RCC_RSR_WWDG1RSTF_Pos,
    LPWRSTF   = RCC_RSR_LPWRRSTF_Pos
  };

  static errors &
  get_instance()
  {
    static errors instance;
    return instance;
  }

  /* Singleton */
  errors(errors const &) = delete;

  void
  operator=(errors const &) = delete;

  etl::bitset<32>::bit_reference
  operator[](reset_flag_pos_t rst)
  {
    return m_reset_flags_raw[static_cast<size_t>(rst)];
  }

  bool
  operator[](reset_flag_pos_t rst) const
  {
    return m_reset_flags_raw[static_cast<size_t>(rst)];
  }

  etl::unordered_map<reset_flag_pos_t, bool, map_size>
  get_reason_of_reset_map(void) const;

  etl::bitset<32>
  get_reason_of_reset_raw(void) const;

private:
  errors();

  void
  get_reason_of_reset(void);

  void
  clear_all(void);

  void
  set(reset_flag_pos_t position, bool value = true);

  etl::unordered_map<reset_flag_pos_t, bool, map_size>
      m_flag_status; // Reset flags

  etl::bitset<32> m_reset_flags_raw; // Raw error code
};