/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <satnogs-comms-proto/satnogs-comms.pb.h>
#include <satnogs-comms/board.hpp>
#include <zephyr/kernel.h>

class test
{
public:
  struct params_work_container
  {
    struct k_work work;
    int           test_id;
    uint32_t      param0;
    uint32_t      param1;
  };

  static test &
  get_instance()
  {
    static test inst;
    return inst;
  }

  /* Singleton */
  test(test const &) = delete;

  void
  operator=(test const &) = delete;

  static void
  exec(struct k_work *item);

  void
  uhf_tx_simple(uint32_t nframes, uint32_t delay_us);

  void
  sband_tx_simple(uint32_t nframes, uint32_t delay_us);

  void
  stop();

  bool
  running() const;

  bool
  test_valid(int id);

private:
  test();

  void
  tx_simple(satnogs::comms::radio::interface iface, uint32_t nframes,
            uint32_t delay_us);

  bool m_running;
  bool m_stop;
};
