/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "test.hpp"
#include "settings.hpp"
#include <zephyr/kernel.h>
#include <zephyr/random/random.h>

namespace sc = satnogs::comms;

static uint8_t buffer[64];

void
test::uhf_tx_simple(uint32_t nframes, uint32_t delay_us)
{
  tx_simple(sc::radio::interface::UHF, nframes, delay_us);
}

void
test::sband_tx_simple(uint32_t nframes, uint32_t delay_us)
{
  tx_simple(sc::radio::interface::SBAND, nframes, delay_us);
}

void
test::tx_simple(sc::radio::interface iface, uint32_t nframes, uint32_t delay_us)
{
  auto &radio = sc::board::get_instance().radio();
  auto &s     = settings::get_instance();
  radio.enable(iface, true);
  radio.set_direction(iface, sc::rf_frontend::dir::TX);
  if (iface == sc::radio::interface::UHF) {
    radio.set_frequency(iface, sc::rf_frontend::dir::TX, s.tx_freq(iface));
  } else {
    radio.set_frequency(iface, sc::rf_frontend::dir::TX, s.tx_freq(iface));
  }
  radio.set_test_fsk(iface);

  for (uint32_t i = 0; i < nframes && m_stop == false; i++) {
    for (size_t j = 0; j < 64; j++) {
      buffer[j] ^= sys_rand32_get() * j;
    }

    radio.tx(iface, buffer, 64);
    k_usleep(std::max(1U, delay_us));
  }
  // Return to RX state
  radio.enable(iface, true);
  radio.set_frequency(iface, sc::rf_frontend::dir::RX, s.rx_freq(iface));
  radio.rx_async(iface);
}

void
test::exec(k_work *item)
{
  auto &t = test::get_instance();
  while (t.m_running) {
    k_sleep(K_MSEC(10));
  }

  t.m_running = true;
  t.m_stop    = false;

  struct params_work_container *params =
      CONTAINER_OF(item, struct params_work_container, work);
  switch (params->test_id) {
  case test_id_UHF_TX_SIMPLE:
    t.uhf_tx_simple(params->param0, params->param1);
    break;
  case test_id_SBAND_TX_SIMPLE:
    t.sband_tx_simple(params->param0, params->param1);
    break;
  }

  t.m_running = false;
  t.m_stop    = false;
}

void
test::stop()
{
  m_stop = true;
}

bool
test::running() const
{
  return m_running;
}

bool
test::test_valid(int id)
{
  switch (id) {
  case test_id_UHF_TX_SIMPLE:
  case test_id_SBAND_TX_SIMPLE:
    return true;
  default:
    return false;
  }
}

test::test() : m_running(false), m_stop(false) {}
