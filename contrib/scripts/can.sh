# ##############################################################################
# SatNOGS-COMMS MCU software
#
# Copyright (C) 2022, Libre Space Foundation <http://libre.space>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GNU General Public License v3.0 or later
# ##############################################################################

#!/bin/bash

can_iface="can0"
device="/dev/ttyACM0"
speed=8


if [[ "$EUID" -ne 0 ]]; then
    echo "This script must be run as root!"
    exit 1
fi

display_usage() {
    echo ""
    echo "This script creates a SLCAN interface and bridges it with a character"
    echo "device of a CAN-to-UART adapter. (e.g Zubax Babel)"
	echo "This script must be run with super-user privileges."
    echo ""
	echo "Usage: $0 -d device -s speed -n can-iface-name"
    echo " -d device : The character device absolute path"
    echo " -s speed  : CAN speed. See slcand for more details"
    echo " -n name   : The name of the CAN interface that will be created and "
    echo "             attached to the character device"
	}

while getopts 'd:n:s:' OPTION;
do
    case "${OPTION}" in
        d) device=${OPTARG};;
        n) can_iface=${OPTARG};;
        s) speed=${OPTARG};;
        ?)
            display_usage
            exit 1
            ;;
    esac
done

# Creates the SLCAN can0 interface and attaches the character device to it


modprobe can-isotp
modprobe slcan

slcand -o -s${speed} -t hw -S 3000000 ${device} ${can_iface}
ip link set up ${can_iface}