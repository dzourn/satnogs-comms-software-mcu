/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

 /* Current, voltage and temperature sensors, internal pull-up */
 &i2c1 {
     pinctrl-0 = <&i2c1_scl_pb8 &i2c1_sda_pb7>;
    pinctrl-names = "default";
    status = "okay";
    clock-frequency = <I2C_BITRATE_STANDARD>;
    /* For bus recovery */
    scl-gpios = <&gpiob 8 (GPIO_ACTIVE_HIGH | GPIO_PULL_UP)>;
    sda-gpios = <&gpiob 7 (GPIO_ACTIVE_HIGH | GPIO_PULL_UP)>;
};

&adc1 {
    pinctrl-0 = <&adc1_inp4_pc4 &adc1_inp8_pc5 &adc1_inp3_pa6 &adc1_inp7_pa7>;
    pinctrl-names = "default";
    status = "okay";
};

/ {
    aliases {
        sensorsi2c = &i2c1;
    };

    leds {
        compatible = "gpio-leds";

        en_pa_uhf: en_pa_uhf {
            gpios = <&gpiob 2 GPIO_ACTIVE_LOW>;
            label = "PA EN UHF";
        };

        en_rx_uhf: en_rx_uhf {
            gpios = <&gpioe 11 GPIO_ACTIVE_LOW>;
            label = "RX EN UHF";
        };

        en_amp_uhf: en_amp_uhf {
            gpios = <&gpioa 1 GPIO_ACTIVE_HIGH>;
            label = "LNA EN UHF";
        };

        /* Enables the load switch of the RF mixer */
        mixer_en: mixer_en {
            gpios = <&gpioc 0 GPIO_ACTIVE_LOW>;
            label = "EN MIXER";
        };
    };

    gpio_keys {
        compatible = "gpio-keys";
        p3v3_rf_pg: p3v3_rf_pg {
            label = "P3V3 RF PGOOD";
            gpios = <&gpioa 9 (GPIO_ACTIVE_HIGH | GPIO_PULL_UP)>;
        };

        p5v_rf_pg: p5v_rf_pg {
            label = "P5V RF PGOOD";
            gpios = <&gpioc 7 (GPIO_ACTIVE_HIGH | GPIO_PULL_UP)>;
        };

        p5v_fpga_pg: p5v_fpga_pg {
            label = "P5V FPGA PGOOD";
            gpios = <&gpiod 9 (GPIO_ACTIVE_HIGH | GPIO_PULL_UP)>;
        };


        alert_pcb_temp: alert_pcb_temp {
            label = "PCB TEMP ALERT";
            gpios = <&gpioe 12 (GPIO_ACTIVE_HIGH | GPIO_PULL_UP)>;
        };

        alert_uhf_pa_temp: alert_uhf_pa_temp {
            label = "UHF PA TEMP ALERT";
            gpios = <&gpioe 8 (GPIO_ACTIVE_HIGH | GPIO_PULL_UP)>;
        };

        alert_sband_pa_temp: alert_sband_pa_temp {
            label = "SBAND PA TEMP ALERT";
            gpios = <&gpiod 14 (GPIO_ACTIVE_HIGH | GPIO_PULL_UP)>;
        };

        flagb_mixer: flagb_mixer {
            label = "FLAGB MIXER";
            gpios = <&gpioe 1 (GPIO_ACTIVE_HIGH | GPIO_PULL_UP)>;
        };

        flagb_sband_pa: flagb_sband_pa {
            label = "FLAGB SBAND PA";
            gpios = <&gpiod 15 (GPIO_ACTIVE_HIGH | GPIO_PULL_UP)>;
        };
    };
};
